"""change tables

Revision ID: daa15e55d76d
Revises: d4e443fc1050
Create Date: 2022-11-02 15:06:09.937034

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'daa15e55d76d'
down_revision = 'd4e443fc1050'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('date_slot')
    op.drop_table('appointment_request')
    op.drop_table('doctors')
    op.drop_table('admins')
    op.drop_table('specialisations')
    op.drop_table('patients')
    op.drop_table('departments')
    op.drop_table('messages')
    # op.drop_table('prescriptions')
    # op.drop_table('procedures')

    op.create_table('admins',
                    sa.Column('id', sa.BigInteger(), primary_key=True),
                    sa.Column('salt', sa.Unicode(), nullable=False),
                    sa.Column('password', sa.String(), nullable=False),
                    sa.Column('username', sa.String(), nullable=False),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_table('patients',
                    sa.Column('id', sa.BigInteger(), primary_key=True),
                    sa.Column('iin', sa.String(), nullable=False),
                    sa.Column('name', sa.String(), nullable=False),
                    sa.Column('surname', sa.String(), nullable=False),
                    sa.Column('middle_name', sa.String(), nullable=False),
                    sa.Column('contact_number', sa.String(), nullable=False),
                    sa.Column('salt', sa.Unicode(), nullable=False),
                    sa.Column('password', sa.String(), nullable=False),
                    sa.Column('government_id', sa.String(), nullable=False, unique=True),

                    sa.Column('day_of_birth', sa.Date(), nullable=False),
                    sa.Column('blood_group', sa.String(), nullable=False),
                    sa.Column('emergency_contact_number', sa.String(), nullable=False),
                    sa.Column('email', sa.String(), nullable=False),
                    sa.Column('address', sa.String(), nullable=False),
                    sa.Column('marital_status', sa.Boolean(), nullable=False),
                    sa.Column('registration_date', sa.Date(), nullable=False),
                    sa.PrimaryKeyConstraint('id')
                    )

    op.create_table('departments',
                    sa.Column('id', sa.BigInteger(), primary_key=True),
                    sa.Column('name', sa.String(), nullable=False),
                    )

    op.create_table('specialisations',
                    sa.Column('id', sa.BigInteger(), primary_key=True),
                    sa.Column('name', sa.String(), nullable=False),
                    )

    op.create_table('doctors',
                    sa.Column('id', sa.BigInteger(), primary_key=True),
                    sa.Column('iin', sa.String(), nullable=False),
                    sa.Column('name', sa.String(), nullable=False),
                    sa.Column('surname', sa.String(), nullable=False),
                    sa.Column('middle_name', sa.String(), nullable=False),
                    sa.Column('contact_number', sa.String(), nullable=False),
                    sa.Column('salt', sa.Unicode(), nullable=False),
                    sa.Column('password', sa.String(), nullable=False),

                    sa.Column('experience', sa.Integer(), nullable=False),
                    sa.Column('price', sa.Float(), nullable=False),
                    sa.Column('rating', sa.Float(), nullable=False),
                    sa.Column('url', sa.String(), nullable=False),
                    sa.Column('day_start', sa.String(), nullable=False),
                    sa.Column('day_end', sa.String(), nullable=False),
                    sa.Column('address', sa.String(), nullable=False),
                    sa.Column('education', sa.String(), nullable=False),
                    sa.Column('photo', sa.String(), nullable=False),
                    sa.Column('category', sa.String(), nullable=False),
                    sa.Column('procedure', sa.String(), nullable=False),
                    sa.Column('department_id', sa.BigInteger(), sa.ForeignKey('departments.id')),
                    sa.Column('specialisation_id', sa.BigInteger(), sa.ForeignKey('specialisations.id')),

                    sa.PrimaryKeyConstraint('id'),

                    )


    op.create_table('appointment_request',
                    sa.Column('id', sa.BigInteger(), primary_key=True),
                    sa.Column('is_active', sa.Boolean(), nullable=False),
                    sa.Column('name', sa.String(), nullable=False),
                    sa.Column('surname', sa.String(), nullable=False),
                    sa.Column('middlename', sa.String(), nullable=False),
                    sa.Column('email', sa.String(), nullable=False),
                    sa.Column('phone', sa.String(), nullable=False),
                    sa.Column('specialisation_id', sa.BigInteger(), sa.ForeignKey('specialisations.id')),
                    sa.Column('doctor_id', sa.BigInteger(), sa.ForeignKey('doctors.id')),
                    sa.Column('status', sa.Integer(), nullable=False),

                    sa.PrimaryKeyConstraint('id'),

                    )

    op.create_table('date_slot',
                    sa.Column('id', sa.BigInteger(), primary_key=True),
                    sa.Column('appointment_request_id', sa.BigInteger(), sa.ForeignKey('appointment_request.id')),
                    sa.Column('start_datetime', sa.String(), nullable=False),
                    sa.Column('end_datetime', sa.String(), nullable=False),

                    sa.PrimaryKeyConstraint('id'),

                    )
    op.create_table('prescriptions',
                    sa.Column('id', sa.BigInteger(), primary_key=True),
                    sa.Column('doctor_id', sa.BigInteger(), sa.ForeignKey('doctors.id')),
                    sa.Column('patient_id', sa.BigInteger(), sa.ForeignKey('patients.id')),
                    sa.Column('start_date', sa.String(), nullable=False),
                    sa.Column('end_date', sa.String(), nullable=False),
                    sa.Column('name', sa.String(), nullable=False),

                    sa.PrimaryKeyConstraint('id'),

                    )

    op.create_table('procedures',
                    sa.Column('id', sa.BigInteger(), primary_key=True),
                    sa.Column('doctor_id', sa.BigInteger(), sa.ForeignKey('doctors.id')),
                    sa.Column('patient_id', sa.BigInteger(), sa.ForeignKey('patients.id')),
                    sa.Column('date', sa.String(), nullable=False),
                    sa.Column('cost', sa.Integer(), nullable=False),
                    sa.Column('name', sa.String(), nullable=False),

                    sa.PrimaryKeyConstraint('id'),

                    )

    op.create_table('messages',
                    sa.Column('id', sa.String(), primary_key=True),
                    sa.Column('sender_iin', sa.String(), nullable=False),
                    sa.Column('receiver_iin', sa.String(), nullable=False),
                    sa.Column('send_time', sa.String(), nullable=False),
                    sa.Column('message_text', sa.String(), nullable=False),

                    sa.PrimaryKeyConstraint('id'),

                    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('patients',
    sa.Column('id', sa.BIGINT(), autoincrement=True, nullable=False),
    sa.Column('salt', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('password', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('username', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('id', name='patients_pkey')
    )
    op.create_table('specialisations',
    sa.Column('id', sa.BIGINT(), server_default=sa.text("nextval('specialisations_id_seq'::regclass)"), autoincrement=True, nullable=False),
    sa.Column('name', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('id', name='specialisations_pkey'),
    postgresql_ignore_search_path=False
    )
    op.create_table('admins',
    sa.Column('id', sa.BIGINT(), autoincrement=True, nullable=False),
    sa.Column('usurname', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('middle_name', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('contact_number', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('salt', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('password', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('day_of_birth', sa.DATE(), autoincrement=False, nullable=False),
    sa.Column('blood_group', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('emergency_contact_number', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('email', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('address', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('marital_status', sa.BOOLEAN(), autoincrement=False, nullable=False),
    sa.Column('registration_date', sa.DATE(), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('id', name='admins_pkey')
    )
    op.create_table('doctors',
    sa.Column('id', sa.BIGINT(), autoincrement=True, nullable=False),
    sa.Column('iin', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('name', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('surname', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('middle_name', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('contact_number', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('salt', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('password', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('experience', sa.INTEGER(), autoincrement=False, nullable=False),
    sa.Column('price', postgresql.DOUBLE_PRECISION(precision=53), autoincrement=False, nullable=False),
    sa.Column('rating', postgresql.DOUBLE_PRECISION(precision=53), autoincrement=False, nullable=False),
    sa.Column('url', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('day_start', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('day_end', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('address', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('education', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('photo', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('category', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('procedure', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('department_id', sa.BIGINT(), autoincrement=False, nullable=True),
    sa.Column('specialisation_id', sa.BIGINT(), autoincrement=False, nullable=True),
    sa.ForeignKeyConstraint(['department_id'], ['departments.id'], name='doctors_department_id_fkey'),
    sa.ForeignKeyConstraint(['specialisation_id'], ['specialisations.id'], name='doctors_specialisation_id_fkey'),
    sa.PrimaryKeyConstraint('id', name='doctors_pkey')
    )
    op.create_table('departments',
    sa.Column('id', sa.BIGINT(), autoincrement=True, nullable=False),
    sa.Column('name', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('id', name='departments_pkey')
    )
    op.create_table('appointment_request',
    sa.Column('id', sa.BIGINT(), primary_key=True),
    sa.Column('is_active', sa.BOOLEAN(), nullable=False),
    sa.Column('name', sa.VARCHAR(), nullable=False),
    sa.Column('surname', sa.VARCHAR(), nullable=False),
    sa.Column('middlename', sa.VARCHAR(), nullable=False),
    sa.Column('email', sa.VARCHAR(), nullable=False),
    sa.Column('phone', sa.VARCHAR(), nullable=False),
    sa.Column('specialisation_id', sa.BIGINT(), autoincrement=False, nullable=True),
    sa.Column('doctor_id', sa.BIGINT(), autoincrement=False, nullable=True),
    sa.ForeignKeyConstraint(['specialisation_id'], ['specialisations.id'], name='appointment_request_specialisation_id_fkey'),
    sa.ForeignKeyConstraint(['doctor_id'], ['doctors.id'], name='appointment_request_doctor_id_fkey'),
    sa.PrimaryKeyConstraint('id'),
    )

    op.create_table('date_slot',
    sa.Column('id', sa.BIGINT(), primary_key=True),
    sa.Column('start_datetime', sa.VARCHAR(), nullable=False),
    sa.Column('end_datetime', sa.VARCHAR(), nullable=False),
    sa.Column('appointment_request_id', sa.BIGINT(), autoincrement=False, nullable=True),
    sa.ForeignKeyConstraint(['appointment_request_id'], ['appointment_request.id'], name='appointment_request_date_slot_id_fkey'),
    sa.PrimaryKeyConstraint('id'),
    )
    op.create_table('messages',
    sa.Column('id', sa.VARCHAR(), primary_key=True),
    sa.Column('sender_iin', sa.VARCHAR(), nullable=False),
    sa.Column('receiver_iin', sa.VARCHAR(), nullable=False),
    sa.Column('send_time', sa.VARCHAR(), nullable=False),
    sa.Column('message_text', sa.VARCHAR(), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    )

    # ### end Alembic commands ###
