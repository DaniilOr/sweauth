"""create tables

Revision ID: d4e443fc1050
Revises: 
Create Date: 2022-11-02 15:01:08.797335

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd4e443fc1050'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table('admins',
                    sa.Column('id', sa.BigInteger(), primary_key=True),
                    sa.Column('salt', sa.Unicode(), nullable=False),
                    sa.Column('password', sa.String(), nullable=False),
                    sa.Column('username', sa.String(), nullable=False),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_table('patients',
                    sa.Column('id', sa.BigInteger(), primary_key=True),
                    sa.Column('iin', sa.String(), nullable=False),
                    sa.Column('name', sa.String(), nullable=False),
                    sa.Column('surname', sa.String(), nullable=False),
                    sa.Column('middle_name', sa.String(), nullable=False),
                    sa.Column('contact_number', sa.String(), nullable=False),
                    sa.Column('salt', sa.Unicode(), nullable=False),
                    sa.Column('password', sa.String(), nullable=False),

                    sa.Column('day_of_birth', sa.Date(), nullable=False),
                    sa.Column('blood_group', sa.String(), nullable=False),
                    sa.Column('emergency_contact_number', sa.String(), nullable=False),
                    sa.Column('email', sa.String(), nullable=False),
                    sa.Column('address', sa.String(), nullable=False),
                    sa.Column('marital_status', sa.Boolean(), nullable=False),
                    sa.Column('registration_date', sa.Date(), nullable=False),
                    sa.PrimaryKeyConstraint('id')
                    )

    op.create_table('departments',
                    sa.Column('id', sa.BigInteger(), primary_key=True),
                    sa.Column('name', sa.String(), nullable=False),
                    )

    op.create_table('specialisations',
                    sa.Column('id', sa.BigInteger(), primary_key=True),
                    sa.Column('name', sa.String(), nullable=False),
                    )

    op.create_table('doctors',
                    sa.Column('id', sa.BigInteger(), primary_key=True),
                    sa.Column('iin', sa.String(), nullable=False),
                    sa.Column('name', sa.String(), nullable=False),
                    sa.Column('surname', sa.String(), nullable=False),
                    sa.Column('middle_name', sa.String(), nullable=False),
                    sa.Column('contact_number', sa.String(), nullable=False),
                    sa.Column('salt', sa.Unicode(), nullable=False),
                    sa.Column('password', sa.String(), nullable=False),

                    sa.Column('experience', sa.Integer(), nullable=False),
                    sa.Column('price', sa.Float(), nullable=False),
                    sa.Column('rating', sa.Float(), nullable=False),
                    sa.Column('url', sa.String(), nullable=False),
                    sa.Column('day_start', sa.String(), nullable=False),
                    sa.Column('day_end', sa.String(), nullable=False),
                    sa.Column('address', sa.String(), nullable=False),
                    sa.Column('education', sa.String(), nullable=False),
                    sa.Column('photo', sa.String(), nullable=False),
                    sa.Column('category', sa.String(), nullable=False),
                    sa.Column('procedure', sa.String(), nullable=False),
                    sa.Column('department_id', sa.BigInteger(), sa.ForeignKey('departments.id')),
                    sa.Column('specialisation_id', sa.BigInteger(), sa.ForeignKey('specialisations.id')),

                    sa.PrimaryKeyConstraint('id'),

                    )

    op.create_table('appointment_request',
                    sa.Column('id', sa.BigInteger(), primary_key=True),
                    sa.Column('is_active', sa.Boolean(), nullable=False),
                    sa.Column('name', sa.String(), nullable=False),
                    sa.Column('surname', sa.String(), nullable=False),
                    sa.Column('middlename', sa.String(), nullable=False),
                    sa.Column('email', sa.String(), nullable=False),
                    sa.Column('phone', sa.String(), nullable=False),
                    sa.Column('specialisation_id', sa.BigInteger(), sa.ForeignKey('specialisations.id')),
                    sa.Column('doctor_id', sa.BigInteger(), sa.ForeignKey('doctors.id')),

                    sa.PrimaryKeyConstraint('id'),

                    )

    op.create_table('date_slot',
                    sa.Column('id', sa.BigInteger(), primary_key=True),
                    sa.Column('appointment_request_id', sa.BigInteger(), sa.ForeignKey('appointment_request.id')),
                    sa.Column('start_datetime', sa.String(), nullable=False),
                    sa.Column('end_datetime', sa.String(), nullable=False),

                    sa.PrimaryKeyConstraint('id'),

                    )
    op.create_table('messages',
                    sa.Column('id', sa.String(), primary_key=True),
                    sa.Column('sender_iin', sa.String(), nullable=False),
                    sa.Column('receiver_iin', sa.String(), nullable=False),
                    sa.Column('send_time', sa.String(), nullable=False),
                    sa.Column('message_text', sa.String(), nullable=False),

                    sa.PrimaryKeyConstraint('id'),

                    )

def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
