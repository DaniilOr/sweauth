﻿using chatapp.web.Models;

namespace chatapp.web.Services
{
    public interface IUserRepository
    {
        Task<ChatUser?> GetUserByIINAsync(string iin);

        Task<IEnumerable<ChatUser>> GetChatUsersAsync();
    }
}
