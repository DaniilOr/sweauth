﻿using chatapp.web.Models;
using Microsoft.EntityFrameworkCore;

namespace chatapp.web.Services
{
    public class MessageRepository : IMessageRepository
    {
        private readonly AppDbContext context;

        public MessageRepository(AppDbContext context)
        {
            this.context = context;
        }

        public async Task<Message> AddMessageAsync(Message message)
        {
            context.Add<Message>(message);
            await context.SaveChangesAsync();
            return message;
        }

        public async Task<IEnumerable<Message>> GetUserMessages(string iin)
        {
            return await context.Messages.Where(m => m.SenderIin == iin || m.ReceiverIin == iin).ToListAsync();
        }
    }
}
