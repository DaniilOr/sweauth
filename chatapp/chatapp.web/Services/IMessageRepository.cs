﻿using chatapp.web.Models;

namespace chatapp.web.Services
{
    public interface IMessageRepository
    {
        public Task<Message> AddMessageAsync(Message message);

        public Task<IEnumerable<Message>> GetUserMessages(string iin);
    }
}
