﻿using chatapp.web.Models;
using Microsoft.EntityFrameworkCore;

namespace chatapp.web.Services
{
    public class UserRepository : IUserRepository
    {
        private readonly AppDbContext context;

        public UserRepository(AppDbContext context)
        {
            this.context = context;
        }
        private async Task<Doctor?> GetDoctorByIINAsync(string iin) => await context.Doctors.FirstOrDefaultAsync(d => d.Iin == iin);

        private async Task<Patient?> GetPatientByIINAsync(string iin) => await context.Patients.FirstOrDefaultAsync(p => p.Iin == iin);

        public async Task<ChatUser?> GetUserByIINAsync(string iin)
        {
            Doctor? doctor = await GetDoctorByIINAsync(iin);
            Patient? patient = await GetPatientByIINAsync(iin);
            if (doctor != null)
            {
                return new ChatUser(doctor);
            }
            if (patient != null)
            {
                return new ChatUser(patient);
            }
            return null;
            //throw new NotImplementedException();
        }

        public async Task<IEnumerable<ChatUser>> GetChatUsersAsync()
        {
            var doctors = await context.Doctors.Select(d => new ChatUser(d)).ToListAsync();
            var patients = await context.Patients.Select(p => new ChatUser(p)).ToListAsync();
            var users = new List<ChatUser>();
            users.AddRange(doctors);
            users.AddRange(patients);
            return users;
        }
    }
}
