﻿using System;
using System.Collections.Generic;

namespace chatapp.web.Models;

public partial class Patient
{
    public long Id { get; set; }

    public string Iin { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string Surname { get; set; } = null!;

    public string MiddleName { get; set; } = null!;

    public string ContactNumber { get; set; } = null!;

    public string Salt { get; set; } = null!;

    public string Password { get; set; } = null!;

    public string GovernmentId { get; set; } = null!;

    public DateOnly DayOfBirth { get; set; }

    public string BloodGroup { get; set; } = null!;

    public string EmergencyContactNumber { get; set; } = null!;

    public string Email { get; set; } = null!;

    public string Address { get; set; } = null!;

    public bool MaritalStatus { get; set; }

    public DateOnly RegistrationDate { get; set; }
}
