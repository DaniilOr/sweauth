﻿using System;
using System.Collections.Generic;

namespace chatapp.web.Models;

public partial class Specialisation
{
    public long Id { get; set; }

    public string Name { get; set; } = null!;

    public virtual ICollection<AppointmentRequest> AppointmentRequests { get; } = new List<AppointmentRequest>();

    public virtual ICollection<Doctor> Doctors { get; } = new List<Doctor>();
}
