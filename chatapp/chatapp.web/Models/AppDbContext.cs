﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace chatapp.web.Models;

public partial class AppDbContext : DbContext
{
    public AppDbContext()
    {
    }

    public AppDbContext(DbContextOptions<AppDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Admin> Admins { get; set; }

    public virtual DbSet<AlembicVersion> AlembicVersions { get; set; }

    public virtual DbSet<AppointmentRequest> AppointmentRequests { get; set; }

    public virtual DbSet<DateSlot> DateSlots { get; set; }

    public virtual DbSet<Department> Departments { get; set; }

    public virtual DbSet<Doctor> Doctors { get; set; }

    public virtual DbSet<Message> Messages { get; set; }

    public virtual DbSet<Patient> Patients { get; set; }

    public virtual DbSet<Specialisation> Specialisations { get; set; }

//    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//        => optionsBuilder.UseNpgsql("Host=localhost:5433;Database=app;Username=postgres;Password=postgres");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Admin>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("admins_pkey");

            entity.ToTable("admins");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Password)
                .HasColumnType("character varying")
                .HasColumnName("password");
            entity.Property(e => e.Salt)
                .HasColumnType("character varying")
                .HasColumnName("salt");
            entity.Property(e => e.Username)
                .HasColumnType("character varying")
                .HasColumnName("username");
        });

        modelBuilder.Entity<AlembicVersion>(entity =>
        {
            entity.HasKey(e => e.VersionNum).HasName("alembic_version_pkc");

            entity.ToTable("alembic_version");

            entity.Property(e => e.VersionNum)
                .HasMaxLength(32)
                .HasColumnName("version_num");
        });

        modelBuilder.Entity<AppointmentRequest>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("appointment_request_pkey");

            entity.ToTable("appointment_request");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.DoctorId).HasColumnName("doctor_id");
            entity.Property(e => e.Email)
                .HasColumnType("character varying")
                .HasColumnName("email");
            entity.Property(e => e.IsActive).HasColumnName("is_active");
            entity.Property(e => e.Middlename)
                .HasColumnType("character varying")
                .HasColumnName("middlename");
            entity.Property(e => e.Name)
                .HasColumnType("character varying")
                .HasColumnName("name");
            entity.Property(e => e.Phone)
                .HasColumnType("character varying")
                .HasColumnName("phone");
            entity.Property(e => e.SpecialisationId).HasColumnName("specialisation_id");
            entity.Property(e => e.Surname)
                .HasColumnType("character varying")
                .HasColumnName("surname");

            entity.HasOne(d => d.Doctor).WithMany(p => p.AppointmentRequests)
                .HasForeignKey(d => d.DoctorId)
                .HasConstraintName("appointment_request_doctor_id_fkey");

            entity.HasOne(d => d.Specialisation).WithMany(p => p.AppointmentRequests)
                .HasForeignKey(d => d.SpecialisationId)
                .HasConstraintName("appointment_request_specialisation_id_fkey");
        });

        modelBuilder.Entity<DateSlot>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("date_slot_pkey");

            entity.ToTable("date_slot");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AppointmentRequestId).HasColumnName("appointment_request_id");
            entity.Property(e => e.EndDatetime)
                .HasColumnType("character varying")
                .HasColumnName("end_datetime");
            entity.Property(e => e.StartDatetime)
                .HasColumnType("character varying")
                .HasColumnName("start_datetime");

            entity.HasOne(d => d.AppointmentRequest).WithMany(p => p.DateSlots)
                .HasForeignKey(d => d.AppointmentRequestId)
                .HasConstraintName("date_slot_appointment_request_id_fkey");
        });

        modelBuilder.Entity<Department>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("departments_pkey");

            entity.ToTable("departments");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Name)
                .HasColumnType("character varying")
                .HasColumnName("name");
        });

        modelBuilder.Entity<Doctor>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("doctors_pkey");

            entity.ToTable("doctors");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Address)
                .HasColumnType("character varying")
                .HasColumnName("address");
            entity.Property(e => e.Category)
                .HasColumnType("character varying")
                .HasColumnName("category");
            entity.Property(e => e.ContactNumber)
                .HasColumnType("character varying")
                .HasColumnName("contact_number");
            entity.Property(e => e.DayEnd)
                .HasColumnType("character varying")
                .HasColumnName("day_end");
            entity.Property(e => e.DayStart)
                .HasColumnType("character varying")
                .HasColumnName("day_start");
            entity.Property(e => e.DepartmentId).HasColumnName("department_id");
            entity.Property(e => e.Education)
                .HasColumnType("character varying")
                .HasColumnName("education");
            entity.Property(e => e.Experience).HasColumnName("experience");
            entity.Property(e => e.Iin)
                .HasColumnType("character varying")
                .HasColumnName("iin");
            entity.Property(e => e.MiddleName)
                .HasColumnType("character varying")
                .HasColumnName("middle_name");
            entity.Property(e => e.Name)
                .HasColumnType("character varying")
                .HasColumnName("name");
            entity.Property(e => e.Password)
                .HasColumnType("character varying")
                .HasColumnName("password");
            entity.Property(e => e.Photo)
                .HasColumnType("character varying")
                .HasColumnName("photo");
            entity.Property(e => e.Price).HasColumnName("price");
            entity.Property(e => e.Procedure)
                .HasColumnType("character varying")
                .HasColumnName("procedure");
            entity.Property(e => e.Rating).HasColumnName("rating");
            entity.Property(e => e.Salt)
                .HasColumnType("character varying")
                .HasColumnName("salt");
            entity.Property(e => e.SpecialisationId).HasColumnName("specialisation_id");
            entity.Property(e => e.Surname)
                .HasColumnType("character varying")
                .HasColumnName("surname");
            entity.Property(e => e.Url)
                .HasColumnType("character varying")
                .HasColumnName("url");

            entity.HasOne(d => d.Department).WithMany(p => p.Doctors)
                .HasForeignKey(d => d.DepartmentId)
                .HasConstraintName("doctors_department_id_fkey");

            entity.HasOne(d => d.Specialisation).WithMany(p => p.Doctors)
                .HasForeignKey(d => d.SpecialisationId)
                .HasConstraintName("doctors_specialisation_id_fkey");
        });

        modelBuilder.Entity<Message>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("messages_pkey");

            entity.ToTable("messages");

            entity.Property(e => e.Id)
                .HasColumnType("character varying")
                .HasColumnName("id");
            entity.Property(e => e.MessageText)
                .HasColumnType("character varying")
                .HasColumnName("message_text");
            entity.Property(e => e.ReceiverIin)
                .HasColumnType("character varying")
                .HasColumnName("receiver_iin");
            entity.Property(e => e.SendTime)
                .HasColumnType("character varying")
                .HasColumnName("send_time");
            entity.Property(e => e.SenderIin)
                .HasColumnType("character varying")
                .HasColumnName("sender_iin");
        });

        modelBuilder.Entity<Patient>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("patients_pkey");

            entity.ToTable("patients");

            entity.HasIndex(e => e.GovernmentId, "patients_government_id_key").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Address)
                .HasColumnType("character varying")
                .HasColumnName("address");
            entity.Property(e => e.BloodGroup)
                .HasColumnType("character varying")
                .HasColumnName("blood_group");
            entity.Property(e => e.ContactNumber)
                .HasColumnType("character varying")
                .HasColumnName("contact_number");
            entity.Property(e => e.DayOfBirth).HasColumnName("day_of_birth");
            entity.Property(e => e.Email)
                .HasColumnType("character varying")
                .HasColumnName("email");
            entity.Property(e => e.EmergencyContactNumber)
                .HasColumnType("character varying")
                .HasColumnName("emergency_contact_number");
            entity.Property(e => e.GovernmentId)
                .HasColumnType("character varying")
                .HasColumnName("government_id");
            entity.Property(e => e.Iin)
                .HasColumnType("character varying")
                .HasColumnName("iin");
            entity.Property(e => e.MaritalStatus).HasColumnName("marital_status");
            entity.Property(e => e.MiddleName)
                .HasColumnType("character varying")
                .HasColumnName("middle_name");
            entity.Property(e => e.Name)
                .HasColumnType("character varying")
                .HasColumnName("name");
            entity.Property(e => e.Password)
                .HasColumnType("character varying")
                .HasColumnName("password");
            entity.Property(e => e.RegistrationDate).HasColumnName("registration_date");
            entity.Property(e => e.Salt)
                .HasColumnType("character varying")
                .HasColumnName("salt");
            entity.Property(e => e.Surname)
                .HasColumnType("character varying")
                .HasColumnName("surname");
        });

        modelBuilder.Entity<Specialisation>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("specialisations_pkey");

            entity.ToTable("specialisations");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Name)
                .HasColumnType("character varying")
                .HasColumnName("name");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
