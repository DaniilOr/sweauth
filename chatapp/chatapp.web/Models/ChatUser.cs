﻿namespace chatapp.web.Models
{
    public record ChatUser
    {
        public ChatUser(string name, string surname, string middlename, string iin, string role, string contactNumber)
        {
            Name = name;
            Surname = surname;
            Middlename = middlename;
            Iin = iin;
            Role = role;
            ContactNumber = contactNumber;
        }

        public ChatUser(Doctor doctor)
        {
            Name = doctor.Name;
            Surname = doctor.Surname;
            Middlename = doctor.MiddleName;
            Iin = doctor.Iin;
            Role = "doctor";
            ContactNumber = doctor.ContactNumber;
        }

        public ChatUser(Patient patient)
        {
            Name = patient.Name;
            Surname = patient.Surname;
            Middlename = patient.MiddleName;
            Iin = patient.Iin;
            Role = "patient";
            ContactNumber = patient.ContactNumber;
        }

        public string Name { get; set; }
        public string Surname { get; set; }
        public string Middlename { get; set; }
        public string Iin { get; set; }
        public string Role { get; set; }
        public string ContactNumber { get; set; }
    }
}
