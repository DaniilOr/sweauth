﻿using System;
using System.Collections.Generic;

namespace chatapp.web.Models;

public partial class DateSlot
{
    public long Id { get; set; }

    public long? AppointmentRequestId { get; set; }

    public string StartDatetime { get; set; } = null!;

    public string EndDatetime { get; set; } = null!;

    public virtual AppointmentRequest? AppointmentRequest { get; set; }
}
