﻿using System;
using System.Collections.Generic;

namespace chatapp.web.Models;

public partial class Doctor
{
    public long Id { get; set; }

    public string Iin { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string Surname { get; set; } = null!;

    public string MiddleName { get; set; } = null!;

    public string ContactNumber { get; set; } = null!;

    public string Salt { get; set; } = null!;

    public string Password { get; set; } = null!;

    public int Experience { get; set; }

    public double Price { get; set; }

    public double Rating { get; set; }

    public string Url { get; set; } = null!;

    public string DayStart { get; set; } = null!;

    public string DayEnd { get; set; } = null!;

    public string Address { get; set; } = null!;

    public string Education { get; set; } = null!;

    public string Photo { get; set; } = null!;

    public string Category { get; set; } = null!;

    public string Procedure { get; set; } = null!;

    public long? DepartmentId { get; set; }

    public long? SpecialisationId { get; set; }

    public virtual ICollection<AppointmentRequest> AppointmentRequests { get; } = new List<AppointmentRequest>();

    public virtual Department? Department { get; set; }

    public virtual Specialisation? Specialisation { get; set; }
}
