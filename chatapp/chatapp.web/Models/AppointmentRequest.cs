﻿using System;
using System.Collections.Generic;

namespace chatapp.web.Models;

public partial class AppointmentRequest
{
    public long Id { get; set; }

    public bool IsActive { get; set; }

    public string Name { get; set; } = null!;

    public string Surname { get; set; } = null!;

    public string Middlename { get; set; } = null!;

    public string Email { get; set; } = null!;

    public string Phone { get; set; } = null!;

    public long? SpecialisationId { get; set; }

    public long? DoctorId { get; set; }

    public virtual ICollection<DateSlot> DateSlots { get; } = new List<DateSlot>();

    public virtual Doctor? Doctor { get; set; }

    public virtual Specialisation? Specialisation { get; set; }
}
