﻿using System;
using System.Collections.Generic;

namespace chatapp.web.Models;

public partial class Admin
{
    public long Id { get; set; }

    public string Salt { get; set; } = null!;

    public string Password { get; set; } = null!;

    public string Username { get; set; } = null!;
}
