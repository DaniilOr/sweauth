﻿using System;
using System.Collections.Generic;

namespace chatapp.web.Models;

public partial class Message
{
    public Message(string senderIin, string receiverIin, string messageText)
    {
        Id= Guid.NewGuid().ToString();
        SendTime = DateTime.UtcNow.ToString();
        SenderIin = senderIin;
        ReceiverIin = receiverIin;
        MessageText = messageText;
    }

    public string Id { get; set; } = null!;

    public string SenderIin { get; set; } = null!;

    public string ReceiverIin { get; set; } = null!;

    public string SendTime { get; set; } = null!;

    public string MessageText { get; set; } = null!;
}
