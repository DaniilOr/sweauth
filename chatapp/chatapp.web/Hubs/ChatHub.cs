﻿using chatapp.web.Models;
using chatapp.web.Services;
using Microsoft.AspNetCore.SignalR;

namespace chatapp.web.Hubs
{
    public class ChatHub: Hub
    {
        private readonly IUserRepository userRepository;
        private readonly IDictionary<string, ChatUser> connections;
        private readonly IMessageRepository messageRepository;

        public ChatHub(IUserRepository userRepository, IDictionary<string, ChatUser> connections, IMessageRepository messageRepository)
        {
            this.userRepository = userRepository;
            this.connections = connections;
            this.messageRepository = messageRepository;
        }

        public override async Task OnConnectedAsync()
        {
            var iin = GetUserIin();
            var user = await userRepository.GetUserByIINAsync(iin);
            if (user != null)
            {
                await SendUsersAsync();
                await SendHistoryAsync();
                await Groups.AddToGroupAsync(Context.ConnectionId, iin);
                connections.Add(Context.ConnectionId, user);
                await SendActiveUsersAsync();
            }
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception? exception)
        {
            if (connections.ContainsKey(Context.ConnectionId))
            {
                connections.Remove(Context.ConnectionId);
                await SendActiveUsersAsync();
            }
            await base.OnDisconnectedAsync(exception);
        }

        public async Task SendActiveUsersAsync()
        {
            var users = connections.Values.ToHashSet<ChatUser>();
            await Clients.All.SendAsync("ReceiveActiveUsers", users.ToList<ChatUser>());
        }

        public async Task SendUsersAsync()
        {
            var users = await userRepository.GetChatUsersAsync();
            await Clients.Caller.SendAsync("ReceiveUsers", users);
        }

        public async Task SendMessageAsync(string receiverIIN, string messageText)
        {
            var senderIIN = GetUserIin();

            var message = await messageRepository.AddMessageAsync(new Message(senderIIN, receiverIIN, messageText));

            var strings = new HashSet<string> { senderIIN, receiverIIN };

            await Clients.Groups(new List<string>(strings)).SendAsync("ReceiveMessage", message);
        }

        public async Task SendHistoryAsync()
        {
            var iin = GetUserIin();
            var messages = await messageRepository.GetUserMessages(iin);
            await Clients.Caller.SendAsync("ReceiveHistory", messages);
        }


        private string GetUserIin() => Context.GetHttpContext()?.Request?.Query["user"].ToString().Trim() ?? "";
    }
}
