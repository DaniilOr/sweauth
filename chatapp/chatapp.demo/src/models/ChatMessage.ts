export interface ChatMessage{
    id: string,
    senderIin: string,
    receiverIin: string,
    messageText: string,
    sendDate: string
}