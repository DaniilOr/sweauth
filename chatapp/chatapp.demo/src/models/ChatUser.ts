export interface ChatUser {
    name: string,
    surname: string,
    middlename: string,
    iin: string,
    role: string
    contactNumber: string;
}