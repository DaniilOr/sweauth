import { FC, useState } from 'react';
import './App.css';
import { ChatMessage, ChatUser } from './models';
import { HubConnection, HubConnectionBuilder, LogLevel } from '@microsoft/signalr';
import { Users, Messages } from './components';

const App: FC = () => {
    const [user, setUser] = useState<string>("")
    const [users, setUsers] = useState<ChatUser[]>([])
    const [activeUsers, setActiveUsers] = useState<ChatUser[]>([])
    const [messages, setMessages] = useState<ChatMessage[]>([]);
    const [message, setMessage] = useState("");
    const [receiver, setReceiver] = useState("");
    const [connection, setConnection] = useState<HubConnection>()

    const baseUrl = `${process.env.REACT_APP_BASE_URL}/chat`;

    const connect = async () => {
        try {
            const connection = new HubConnectionBuilder()
                .withUrl(`${baseUrl}/?user=${user}`)
                .configureLogging(LogLevel.Information)
                .build();

            connection.on("ReceiveMessage", (message: ChatMessage) => {
                console.log("ReceiveMessage", message);
                setMessages(messages => [...messages, message]);
            });

            connection.on("ReceiveHistory", (history: ChatMessage[]) => {
                console.log("ReceiveHistory", history);
                setMessages(history);
            });

            connection.on("ReceiveUsers", (users: ChatUser[]) => {
                console.log("ReceiveUsers", users);
                setUsers(users);
            });

            connection.on("ReceiveActiveUsers", (users: ChatUser[]) => {
                console.log("ReceiveActiveUsers", users);
                setActiveUsers(users);
            });

            await connection.start();

            setConnection(connection);

        } catch (error) {
            alert("Failed to connect");
            console.error(error);
        }
    }

    const sendMessage = async (receiver: string, message: string) => {
        if (connection !== undefined) {
            try {
                await connection.invoke("SendMessageAsync", receiver, message);
            } catch (e) {
                console.log(e);
            }
        }
    }

    const closeConnection = async () => {
        if (connection !== undefined) {
            try {
                await connection.stop();
                setConnection(undefined);
            } catch (e) {
                console.log(e);
            }
        }
    }

    const updateMessages = async () => {
        if (connection !== undefined) {
            try {
                await connection.invoke("UpdateMessages");
            } catch (error) {
                console.error(error)
            }
        }
    }

    const updateUsers = async () => {
        if (connection !== undefined) {
            try {
                await connection.invoke("UpdateUsers")
            } catch (error) {
                console.error(error)
            }
        }
    }

    return (
        <div className="App">
            <h2>My Chat</h2>
            <div>
                <label>
                    <strong>IIN: </strong>
                    <input type="text" disabled={connection !== undefined} value={user} onChange={(e) => setUser(e.target.value)} />
                </label>
            </div>
            {user === "" ?
                <div>Please enter your number</div> :
                <>
                    <button disabled={connection !== undefined} type="button" onClick={connect}>Connect</button>
                    <button disabled={connection === undefined} type="button" onClick={closeConnection}>Close</button>
                </>
            }
            {connection === undefined ? null :
                <>
                    <hr />
                    <div>
                        <label>
                            <strong>Message: </strong>
                            <input type="text" value={message} onChange={(e) => setMessage(e.target.value)} />
                        </label>
                        <br />
                        <label>
                            <strong>Receiver: </strong>
                            <input type="text" value={receiver} onChange={(e) => setReceiver(e.target.value)} />
                        </label>
                    </div>
                    <button type="button" onClick={() => sendMessage(receiver, message)}>Send</button>
                    <hr />
                    <button type="button" onClick={updateUsers}>Update users</button>
                    <button type="button" onClick={updateMessages}>Update messages</button>
                    <Users users={users} activeUsers={activeUsers} />
                    <Messages messages={messages} />
                </>
            }
        </div>
    );
}

export default App;
