import { FC } from "react";
import { ChatMessage } from "../models/ChatMessage";

export interface MessagesProps {
    messages: ChatMessage[]
}

export const Messages: FC<MessagesProps> = ({ messages }) => {
    return (
        <>
            <h3>Messages:</h3>
            {messages.length === 0 ? null :
                <ul>
                    {messages.map(message => <li key={message.id}><strong>{message.senderIin} to {message.receiverIin}: </strong>{message.messageText}</li>)}
                </ul>
            }
        </>
    )
}