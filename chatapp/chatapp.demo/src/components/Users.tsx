import { FC } from "react";
import { ChatUser } from "../models/ChatUser";

export interface UsersProps {
  users: ChatUser[];
  activeUsers: ChatUser[];
}

export const Users: FC<UsersProps> = ({ users, activeUsers }) => {
  return (
    <>
      <h3>Users:</h3>
      {users.length === 0 ? null : (
        <ol>
          {users.map((user) => (
            <li key={user.iin}>
              <strong>{user.role}: </strong>
              {user.name} {user.surname}{" "}
              {activeUsers.find((u) => u.iin === user.iin) ? "*" : null}
            </li>
          ))}
        </ol>
      )}
    </>
  );
};
