from .. import auth_service
from fastapi import APIRouter, status, Body
from fastapi.responses import JSONResponse

from ..models import AppointmentRequest
from ..schemas import PatientCreate, Specialisation, PatientPublic, DoctorPublic, Department, DoctorCreate, \
    PrescriptionCreate, Specialisation, AdminPublic, AdminLogin, Admin, AccessToken, AdminCreate, PatientInDB, DoctorInDB, \
    OthersLogin, DateRange, AppointmentRequestCreate, AppointmentRequestUpdate, PatientListed, ProcedureCreate, History, Report, Message
from ..crud import create_patient, create_doctor, create_department, create_specialisation, create_schedule, \
    get_user_by_username, add_admin, get_departments, get_specs, get_schedules, get_all_patients, \
    upd_patient, upd_doctor, get_all_doctors, get_doctor_by_iin, get_patient_by_iin, search_doctors, \
    create_appointment_request, get_active_requests, change_request_status, get_appointments, prescribe_medication, \
    get_prescriptions_by_doctor, get_prescriptions_by_patient, prescribe_procedure, get_procedures_by_patient, \
    get_procedures_by_doctor, get_appointments_by_email, get_patients_for_period, get_medications_for_period, \
    get_procedures_for_period, get_appointments_for_period, get_user_messages, send_message
from fastapi import HTTPException
import pandas as pd
import typing as t

router = APIRouter()


def validate(token, role='administrator'):
    return auth_service.decode_access_token(token).get('role', '') == role

def get_name(token):
    return auth_service.decode_access_token(token).get('username', '')


@router.post(
    "/create_patinet",
    tags=["patient registration"],
    description="Register the Patient",
    response_model=PatientPublic,
)
async def patient_create(user: PatientCreate):
    if validate(user.access_token):
        return await create_patient(user)
    else:
        raise HTTPException(404, "administrator not found")


# just to check that a user is updated
@router.get(
    '/get_patients',
    tags=["test"],
    description="Get list of all patients",
    response_model=t.List[PatientListed]
)
async def get_patients() -> t.List[PatientListed]:
    return await get_all_patients()


@router.put(
    '/update_patient',
    tags=["update", "patients"],
    description="Update patient details",
    response_model=PatientInDB
)
async def update_patient(patient: PatientCreate) -> PatientInDB:
    if validate(patient.access_token):
        return await upd_patient(patient)
    else:
        raise HTTPException(404, "administrator not found")


@router.put(
    '/update_doctor',
    tags=["update", "doctors"],
    description="Update doctor details",
    response_model=DoctorInDB
)
async def update_doctor(doctor: DoctorCreate) -> DoctorInDB:
    if validate(doctor.access_token):
        return await upd_doctor(doctor)
    else:
        raise HTTPException(404, "administrator not found")


# test if doctors are updated
@router.get(
    '/get_doctors',
    tags=["test"],
    description="Get list of all doctors",
    response_model=t.List[DoctorPublic]
)
async def get_doctors() -> t.List[DoctorPublic]:
    return await get_all_doctors()


@router.get(
    '/get_doctors/{search}',
    tags=["test"],
    description="Get list of doctors by search params",
    response_model=t.List[DoctorPublic]
)
async def get_doctors_search(search) -> t.List[DoctorPublic]:
    return await search_doctors(search)

@router.get(
    '/get_doctor_by_iin/{iin}',
    tags=["test", "doctor"],
    description="Get doctor by iin",
    response_model=DoctorPublic
)
async def get_doctor_by_iin_handler(iin) -> DoctorPublic:
    return await get_doctor_by_iin(iin)

@router.get(
    '/get_patient_by_iin/{iin}',
    tags=["test", "patient"],
    description="Get patient by iin",
    response_model=PatientListed
)
async def get_patient_by_iin_handler(iin) -> PatientListed:
    return await get_patient_by_iin(iin, True)


@router.post(
    "/create_doctor",
    tags=["patient registration"],
    description="Register the Patient",
    response_model=DoctorPublic,
)
async def doctor_create(user: DoctorCreate):
    if validate(user.access_token):
        return await create_doctor(user)
    else:
        raise HTTPException(404, "administrator not found")


## Тут костыли, чтобы пока можно было создать департаменты и тд
@router.post(
    "/create_department",
    tags=["department registration"],
    description="Register new department",
    response_model=Department,
)
async def department_create(dep: Department):
    return await create_department(dep)


@router.post(
    "/create_appointment_request",
    tags=["appointment request"],
    description="Request appointment",
    response_model=AppointmentRequestCreate,
)
async def appointment_create(arc: AppointmentRequestCreate):
    return await create_appointment_request(arc)


@router.get(
    "/get_active_requests",
    tags=["appointment request"],
    description="Get active appointment requests",
    response_model=t.List[AppointmentRequestUpdate],
)
async def active_requests_get() -> t.List[AppointmentRequestUpdate]:
    return await get_active_requests()


@router.post(
    "/create_specialisation",
    tags=["specialisation registration"],
    description="Register new spec",
    response_model=Specialisation,
)
async def department_create(spec: Specialisation):
    return await create_specialisation(spec)


@router.post(
    '/login',
    tags=["user login"],
    description="Log in the User",
    response_model=AdminPublic
)
async def admin_login(user: AdminLogin) -> AdminPublic:
    found_user = await get_user_by_username(user_name=user.username)
    if auth_service.verify_password(password=user.password, salt=found_user.salt, hashed_pw=found_user.password):
        token = auth_service.create_access_token_for_user(user=found_user)
        access_token = AccessToken(access_token=token, token_type='bearer')
        return AdminPublic(**found_user.dict(), access_token=access_token)
    else:
        raise HTTPException(404, "Admin not found")


@router.post(
    '/add_admin',
    tags=["user login"],
    description="Add admin",
    response_model=Admin
)
async def admin_registration(user: AdminCreate) -> Admin:
    return await add_admin(user)


@router.get(
    '/get_departments',
    tags=["get", "list", "departments"],
    description="Get list of departments",
    response_model=t.List[Department]
)
async def get_departments_list() -> t.List[Department]:
    return await get_departments()


@router.get(
    '/get_specialisations',
    tags=["get", "list", "specialisations"],
    description="Get list of specialisations",
    response_model=t.List[Department]
)
async def get_specialisations_list() -> t.List[Specialisation]:
    return await get_specs()


@router.get(
    '/get_schedules/{doctor_id}',
    tags=["get", "list", "schedules"],
    description="Get list of doctor schedules",
    response_model=t.List[DateRange]
)
async def get_schedules_list(doctor_id=0) -> t.List[DateRange]:
    return await get_schedules(doctor_id)


@router.post(
    '/login/{role}',
    tags=["post login"],
    description="Login for patients and doctors",
    response_model=t.Union[PatientPublic, DoctorPublic]
)
async def login_by_role(role: str, user: OthersLogin) -> t.Union[PatientPublic, DoctorPublic]:
    if role == 'patient':
        found_user = await get_patient_by_iin(iin=user.iin)
    elif role == 'doctor':
        found_user = await get_doctor_by_iin(iin=user.iin)
    else:
        raise HTTPException(404, "Method not found")

    if auth_service.verify_password(password=user.password, salt=found_user.salt, hashed_pw=found_user.password):
        token = auth_service.create_access_token_for_user(user=found_user)
        access_token = AccessToken(access_token=token, token_type='bearer')
        return PatientPublic(**found_user.dict(), access_token=access_token) if role == "patient" else DoctorPublic(
            **found_user.dict(), access_token=access_token)
    else:
        raise HTTPException(404, "Admin not found")


@router.post(
    '/mark_appointment',
    tags=["appointment request"],
    description="Change appointment status",
    response_model=AppointmentRequestUpdate,
)
async def accept_appointment(appointment: AppointmentRequestUpdate, token: str, status: int):
    if validate(token):
        return await change_request_status(appointment, status)
    else:
        raise HTTPException(405, 'No permission')


@router.get(
    '/get_doctor_appointments',
    tags=["appointment request"],
    description="Get appointments for doctor",
    response_model=t.List[AppointmentRequestUpdate],
)
async def get_doctor_appointments(doctor_id: int):
    return await get_appointments(doctor_id)


@router.post(
    '/prescribe_medication',
    tags=["prescriptions"],
    description="Prescribe medication",
    response_model=PrescriptionCreate,
)
async def post_prescribe_medication(prescription: PrescriptionCreate, token: str):
    # return await prescribe_medication(prescription)
    if validate(token, "doctor"):
        return await prescribe_medication(prescription)
    else:
        raise HTTPException(405, 'No permission')


@router.get(
    '/get_prescriptions',
    tags=["prescriptions"],
    description="Prescribe medication",
    response_model=t.List[PrescriptionCreate],
)
async def get_prescriptions_handler(id: int, token: str):
    if validate(token, "doctor"):
        return await get_prescriptions_by_doctor(id)
    elif validate(token, "patient"):
        return await get_prescriptions_by_patient(id)
    else:
        raise HTTPException(405, 'No permission')


@router.post(
    '/prescribe_procedure',
    tags=["prescriptions"],
    description="Prescribe procedure",
    response_model=ProcedureCreate,
)
async def prescribe_procedure_handler(procedure: ProcedureCreate, token: str):
    # return await prescribe_procedure(procedure)
    if validate(token, "doctor"):
        return await prescribe_procedure(procedure)
    else:
        raise HTTPException(405, 'No permission')


@router.get(
    '/get_procedures',
    tags=["prescriptions"],
    description="Get procedures",
    response_model=t.List[ProcedureCreate],
)
async def get_procedures(id: int, token: str):
    if validate(token, "doctor"):
        return await get_procedures_by_doctor(id)
    elif validate(token, "patient"):
        return await get_procedures_by_patient(id)
    else:
        raise HTTPException(405, 'No permission')


@router.get(
    '/get_procedures_patient',
    tags=["procedures"],
    description="Get procedures patient",
    response_model=t.List[ProcedureCreate],
)
async def get_procedures(id: int):
    return await get_procedures_by_patient(id)

@router.get(
    '/get_prescriptions_patient',
    tags=["prescriptions"],
    description="Get prescriptions patient",
    response_model=t.List[PrescriptionCreate],
)
async def get_procedures(id: int):
    return await get_prescriptions_by_patient(id)


@router.get(
    '/get_appointments_by_email_patient',
    tags=["prescriptions"],
    description="Get prescriptions patient",
    response_model=t.List[AppointmentRequestUpdate],
)
async def get_procedures(email: str):
    return await get_appointments_by_email(email)


@router.get(
    '/get_treatment_history',
    tags=["history"],
    description="Get treatment history",
    response_model=History,
)
async def get_treatment_history(token: str):
    if validate(token, 'patient'):
        iin = auth_service.decode_access_token(token).get('username', '')
        patient = await get_patient_by_iin(iin, True)
        procedures = await get_procedures_by_patient(patient.id)
        medications = await get_prescriptions_by_patient(patient.id)

        appointments = await get_appointments_by_email(patient.email)

        return History(
            procedures=procedures,
            medications=medications,
            appointments=appointments
        )


@router.get(
    '/get_treatment_history_patient_iin',
    tags=["history"],
    description="Get patient treatment history",
    response_model=History,
)
async def get_treatment_history_patient_iin(iin: str):
    patient = await get_patient_by_iin(iin, True)
    procedures = await get_procedures_by_patient(patient.id)
    medications = await get_prescriptions_by_patient(patient.id)

    appointments = await get_appointments_by_email(patient.email)

    return History(
        procedures=procedures,
        medications=medications,
        appointments=appointments
    )

@router.get(
    '/get_treatment_history_doctor_iin',
    tags=["history"],
    description="Get doctor treatment history",
    response_model=History,
)
async def get_treatment_history_patient_iin(iin: str):
    doctor = await get_doctor_by_iin(iin)
    procedures = await get_procedures_by_doctor(doctor.id)
    medications = await get_prescriptions_by_doctor(doctor.id)

    appointments = await get_appointments(doctor.id)

    return History(
        procedures=procedures,
        medications=medications,
        appointments=appointments
    )


# may need to send with users name, maybe new schema MessagePublic
@router.get(
    '/get_messages/{user_iin}',
    tags=["get", "list", "messages"],
    description="Get list of user messages",
    response_model=t.List[Message]
)
async def get_messages_list(iin=0) -> t.List[Message]:
    return await get_user_messages(iin)

@router.post(
    "/send_message",
    tags=["send message"],
    description="Send message",
    response_model=Message,
)
async def post_message(message: Message):
    return await send_message(message)



@router.get(
    '/patients_for_period',
    tags=["report"],
    description="Get patients for period",
    response_model=Report,
)
async def patients_for_period(start_date: str, end_date: str):
    patients_ = await get_patients_for_period(start_date, end_date)
    patients = pd.DataFrame.from_records([x.dict() for x in patients_])
    dates = pd.date_range(start_date, end_date).strftime('%Y-%m-%d')
    patient_counts = patients.groupby('registration_date')['email'].count().reset_index()
    patient_counts['registration_date'] = patient_counts['registration_date'].apply(lambda x: x.strftime('%Y-%m-%d'))
    patient_counts = patient_counts.rename({'registration_date': 'date', 'email': 'count'}, axis=1).set_index('date')

    patient_counts_chart = pd.DataFrame({"date": dates, "count": [0] * len(dates)}).set_index('date')

    patient_counts_chart.loc[patient_counts_chart.index.isin(patient_counts.index), 'count'] = patient_counts['count']
    procedures_sums_chart = pd.DataFrame()
    procedures_by_name = pd.DataFrame()
    medications_by_name = pd.DataFrame()
    prescriptions_counts_chart = pd.DataFrame()
    procedures_counts_chart = pd.DataFrame()

    procedures_ = await get_procedures_for_period(start_date, end_date)
    if len(procedures_) > 0:
        procedures = pd.DataFrame.from_records([x.dict() for x in procedures_])
        procedures_counts = procedures.groupby('date')['cost'].count()

        procedures_sums = procedures.groupby('date')['cost'].sum()
        procedures_counts_chart = pd.DataFrame({"date": dates, "count": [0] * len(dates)}).set_index('date')
        procedures_counts_chart.loc[procedures_counts_chart.index.isin(procedures_counts.index), 'count'] = procedures_counts

        procedures_sums_chart = pd.DataFrame({"date": dates, "sum": [0] * len(dates)}).set_index('date')
        procedures_sums_chart.loc[procedures_counts_chart.index.isin(procedures_counts.index), 'sum'] = procedures_sums


        procedures_by_name = procedures.groupby('name')['cost'].count().reset_index().rename({'cost':'count'}, axis=1).set_index('name')

    prescriptions = await get_medications_for_period(start_date, end_date)
    if len(prescriptions) > 0:

        prescriptions = pd.DataFrame.from_records([x.dict() for x in prescriptions])
        prescriptions_counts = prescriptions.groupby('start_date')['name'].count().reset_index().rename({'name':'count',
                                                                                                         'start_date':'date'}, axis=1).set_index('date')

        prescriptions_counts_chart = pd.DataFrame({"date": dates, "count": [0] * len(dates)}).set_index('date')
        prescriptions_counts_chart.loc[procedures_counts_chart.index.isin(prescriptions_counts.index), 'count'] = \
        prescriptions_counts['count']

        medications_by_name = prescriptions.groupby('name')['start_date'].count().reset_index()
        medications_by_name = medications_by_name.rename({'start_date':'count'}).set_index('name')

        medications_by_name = prescriptions.groupby('name')['start_date'].count().reset_index()
        medications_by_name = medications_by_name.rename({'start_date':'count'}).set_index('name')

    patient_counts_chart = patient_counts_chart.to_dict().get('count', {})
    procedures_sums_chart = procedures_sums_chart.to_dict().get('sum', {})
    procedures_counts_chart = procedures_counts_chart.to_dict().get('count', {})
    procedures_by_name = procedures_by_name.to_dict().get('count', {})
    prescriptions_counts_chart = prescriptions_counts_chart.to_dict().get('count', {})
    medications_by_name = medications_by_name.to_dict().get('start_date', {})

    #await get_appointments_for_period(start_date, end_date)
    return Report(
        patient_counts_chart=patient_counts_chart,
        procedures_sums_chart=procedures_sums_chart,
        procedures_counts_chart=procedures_counts_chart,
        procedures_by_name=procedures_by_name,
        prescriptions_counts_chart=prescriptions_counts_chart,
        medications_by_name=medications_by_name
    )
