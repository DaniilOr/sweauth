import typing
from datetime import datetime, date, timedelta
from typing import Optional, List

from pydantic import BaseModel, EmailStr, constr

class UserModel(BaseModel):
    iin: str
    name: str
    surname: str
    middle_name: str
    contact_number: str

    class Config:
        orm_mode = True


class AccessToken(BaseModel):
    access_token: str
    token_type: str


class Patient(UserModel):
    day_of_birth: date
    blood_group: str
    emergency_contact_number: str
    email: Optional[EmailStr]
    address: str
    marital_status: bool = True
    government_id: str
    registration_date: date = datetime.now()

    password: str


    class Config:
        orm_mode = True


class Doctor(UserModel):
    id: Optional[int]
    experience: int
    price: float
    rating: float
    url: str
    day_start: Optional[str]
    day_end: Optional[str]
    address: str
    education: str
    photo: str = "address/sample"
    category: str
    procedure: Optional[str]

    specialisation_id: int
    department_id: int

    password: str

    class Config:
        orm_mode = True


class DoctorCreate(Doctor):
    password: constr(min_length=7, max_length=100)
    access_token: str

    class Config:
        orm_mode = True


class PatientCreate(Patient):
    password: constr(min_length=7, max_length=100)
    access_token: str

    class Config:
        orm_mode = True


class PatientListed(Patient):
    id: int

    class Config:
        orm_mode = True


class PatientInDB(Patient):
    password: constr(min_length=7, max_length=100)
    salt: str

    class Config:
        orm_mode = True


class UserInDB(UserModel):
    password: constr(min_length=7, max_length=100)
    salt: str

    class Config:
        orm_mode = True


class DoctorInDB(Doctor):
    password: constr(min_length=7, max_length=100)
    salt: str

    class Config:
        orm_mode = True


class UserPublic(UserModel):
    access_token: Optional[AccessToken]

    class Config:
        orm_mode = True


class PatientPublic(Patient):
    access_token: Optional[AccessToken]

    class Config:
        orm_mode = True


class DoctorPublic(Doctor):
    access_token: Optional[AccessToken]

    class Config:
        orm_mode = True


class Message(BaseModel):
    id: str
    sender_iin: str
    receiver_iin: str
    send_time: str
    message_text: str

    class Config:
        orm_mode = True


class UserPasswordUpdate(BaseModel):
    password: constr(min_length=7, max_length=100)
    salt: str

    class Config:
        orm_mode = True


class PatientPasswordUpdate(UserPasswordUpdate):
    pass


class DoctorPasswordUpdate(BaseModel):
    pass


class Department(BaseModel):
    name: str
    id: int

    class Config:
        orm_mode = True


class Specialisation(BaseModel):
    name: str
    id: int

    class Config:
        orm_mode = True


class Schedule(BaseModel):
    id: int

    class Config:
        orm_mode = True


config = {
    "SECRET_KEY": "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7",
    "JWT_ALGORITHM": "HS256",
    "ACCESS_TOKEN_EXPIRE_MINUTES": 30,
    "JWT_TOKEN_PREFIX": "Bearer",
    "JWT_AUDIENCE": "backend:auth",

}


class JWTMeta(BaseModel):
    role: str = "administrator"
    aud: str = config['JWT_AUDIENCE']
    iat: float = datetime.timestamp(datetime.now())
    exp: float = datetime.timestamp(datetime.now() + timedelta(minutes=config['ACCESS_TOKEN_EXPIRE_MINUTES']))


class JWTCreds(BaseModel):
    username: str


class JWTPayload(JWTMeta, JWTCreds):
    pass


class Admin(BaseModel):
    id: int
    username: str
    salt: str
    password: str

    class Config:
        orm_mode = True


class AdminLogin(BaseModel):
    username: str
    password: constr(min_length=7, max_length=100)

    class Config:
        orm_mode = True


class AdminPublic(BaseModel):
    access_token: Optional[AccessToken]
    username: str

    class Config:
        orm_mode = True


class AdminCreate(BaseModel):
    id: int
    username: str
    password: str

    class Config:
        orm_mode = True


class OthersLogin(BaseModel):
    iin: str
    password: constr(min_length=7, max_length=100)

    class Config:
        orm_mode = True


class DateRange(BaseModel):
    start_datetime: str
    end_datetime: str

    # def __init__(self, start_datetime: str, end_datetime: str):
    #     self.start_datetime = start_datetime
    #     self.end_datetime = end_datetime

    class Config:
        orm_mode = True


class AppointmentRequestCreate(BaseModel):
    doctor_id: int
    specialisation_id: int
    name: str
    surname: str
    middlename: str
    email: str
    phone: str
    time_slots: List[DateRange] = []
    is_active: bool = True

    class Config:
        orm_mode = True


class AppointmentRequestUpdate(AppointmentRequestCreate):
    id: int
    class Config:
        orm_mode = True


class PrescriptionCreate(BaseModel):
    doctor_id: int
    patient_id: int

    name: str
    start_date: str
    end_date: str

    class Config:
        orm_mode = True


class ProcedureCreate(BaseModel):
    doctor_id: int
    patient_id: int

    name: str
    date: str
    cost: int

    class Config:
        orm_mode = True


class History(BaseModel):
    medications: typing.Optional[typing.List[PrescriptionCreate]]
    procedures: typing.Optional[typing.List[ProcedureCreate]]
    appointments: typing.Optional[typing.List[AppointmentRequestUpdate]]


class Report(BaseModel):
    patient_counts_chart: typing.Optional[typing.Dict[str, int]]
    procedures_sums_chart: typing.Optional[typing.Dict[str, int]]
    procedures_counts_chart: typing.Optional[typing.Dict[str, int]]
    procedures_by_name: typing.Optional[typing.Dict[str, int]]
    prescriptions_counts_chart: typing.Optional[typing.Dict[str, int]]
    medications_by_name : typing.Optional[typing.Dict[str, int]]