import datetime

from uuid import uuid4

import users.models
from .schemas import PatientPasswordUpdate, PatientCreate, \
    PatientInDB, Department, DoctorInDB, DoctorCreate, Specialisation, Schedule, Admin, AdminCreate, PatientPublic, \
    DoctorPublic, DateRange, AppointmentRequestCreate, AppointmentRequestUpdate, PrescriptionCreate, PatientListed, \
    ProcedureCreate, Message
from users import auth_service
from .models import Patient, db, Doctor, Department as DEP, Specialisation as SPEC, Schedule as SCH, Admin as ADM, \
    AppointmentRequest, DateSlot, Prescriptions, Procedures, Message as MSG
from sqlalchemy import and_, func, select
from app.core.config import settings
from fastapi import HTTPException
import typing as t


async def create_patient(new_user: PatientCreate) -> PatientInDB:
    new_password = auth_service.create_salt_and_hashed_password(plaintext_password=new_user.password)
    new_user_params = new_user.copy(update=new_password.dict())
    new_user_updated = PatientInDB(**new_user_params.dict())
    created_user = await Patient.create(**new_user_updated.dict())
    return PatientInDB.from_orm(created_user)


async def create_doctor(new_user: DoctorCreate) -> DoctorInDB:
    new_password = auth_service.create_salt_and_hashed_password(plaintext_password=new_user.password)
    new_user_params = new_user.copy(update=new_password.dict())
    new_user_updated = DoctorInDB(**new_user_params.dict())
    created_user = await Doctor.create(**new_user_updated.dict())
    return DoctorInDB.from_orm(created_user)


async def create_department(new_department: Department) -> Department:
    old_dep = await DEP.query.where(DEP.name == new_department.name).gino.first()
    if old_dep is not None:
        raise HTTPException(status_code=403, detail="Already exists")

    created_dep = await DEP.create(**new_department.dict())
    return created_dep


async def create_specialisation(new_spec: Specialisation) -> Specialisation:
    old_spec = await SPEC.query.where(SPEC.name == new_spec.name).gino.first()
    if old_spec is not None:
        raise HTTPException(status_code=403, detail="Already exists")
    created_spec = await SPEC.create(**new_spec.dict())
    return created_spec


async def create_schedule(new_schedule: Schedule) -> Schedule:
    created_schedule = await SCH.create(**new_schedule.dict())
    return created_schedule


async def create_appointment_request(arc: AppointmentRequestCreate) -> AppointmentRequestCreate:
    arc_dict = arc.dict()
    del arc_dict['time_slots']
    arc_dict['status'] = 0
    created_arc = await AppointmentRequest.create(**arc_dict)
    for x in arc.time_slots:
        await DateSlot.create(**{"appointment_request_id": created_arc.id,
                                 "start_datetime": x.start_datetime, "end_datetime": x.end_datetime})
    return created_arc


async def get_user_by_username(user_name: str) -> Admin:
    found_user = await ADM.query.where(ADM.username == user_name).gino.first()
    return Admin.from_orm(found_user)


async def add_admin(admin: AdminCreate) -> Admin:
    new_password = auth_service.create_salt_and_hashed_password(plaintext_password=admin.password)
    new_user_params = admin.copy(update=new_password.dict())
    new_user_updated = Admin(**new_user_params.dict())
    created_user = await ADM.create(**new_user_updated.dict())
    return created_user


async def get_departments() -> t.List[Department]:
    return await DEP.query.gino.all()


async def get_specs() -> t.List[Specialisation]:
    return await SPEC.query.gino.all()


async def get_schedules(doctor_id: int) -> t.List[DateRange]:
    appointments = await Schedule.query.where(Schedule.doctor_id == doctor_id).gino.all()
    return list(map(lambda x: DateRange(x.start_datetime, x.end_datetime), appointments))


async def get_all_patients() -> t.List[PatientListed]:
    return await Patient.query.gino.all()


async def get_all_doctors() -> t.List[DoctorPublic]:
    return await Doctor.query.gino.all()


async def search_doctors(search: str) -> t.List[DoctorPublic]:
    doctors = await get_all_doctors()
    specialisations = list(
        filter(lambda x: x.name.lower().__contains__(search.lower()) or search.lower().__contains__(x.name.lower()),
               await get_specs()))
    s_ids = list(map(lambda x: x.id, specialisations))
    result = list(filter(lambda x: ((x.name.lower() != "" and x.name.lower().__contains__(search.lower())) or (
            x.surname.lower() != "" and x.surname.lower().__contains__(search.lower())) or (
                                            x.middle_name.lower() != "" and x.middle_name.lower().__contains__(
                                        search.lower())) or (
                                            x.procedure.lower() != "" and x.procedure.lower().__contains__(
                                        search.lower())) or (x.specialisation_id in s_ids)), doctors))
    return result


async def upd_patient(patient: PatientCreate) -> PatientInDB:
    old_patient = await Patient.query.where(Patient.iin == patient.iin).gino.first()
    if old_patient is None:
        raise HTTPException(404, "patient not found")
    patient = patient.dict()
    del patient['access_token']
    await old_patient.update(**patient).apply()
    return PatientInDB.from_orm(old_patient)


async def upd_doctor(doctor: DoctorCreate) -> DoctorInDB:
    old_doctor = await Doctor.query.where(Doctor.id == doctor.id).gino.first()
    if old_doctor is None:
        raise HTTPException(404, "doctor not found")
    doctor = doctor.dict()
    del doctor['access_token']
    await old_doctor.update(**doctor).apply()
    return DoctorInDB.from_orm(old_doctor)


async def get_patient_by_iin(iin: str, return_id=False) -> t.Union[PatientPublic, PatientListed]:
    found_user = await Patient.query.where(Patient.iin == iin).gino.first()
    return PatientInDB.from_orm(found_user) if not return_id else PatientListed.from_orm(found_user)

async def get_doctor_by_iin(iin: str) -> DoctorPublic:
    found_user = await Doctor.query.where(Doctor.iin == iin).gino.first()
    return DoctorInDB.from_orm(found_user)


async def get_active_requests() -> t.List[AppointmentRequestUpdate]:
    active_requests = await AppointmentRequest.query.where(and_(AppointmentRequest.is_active,
                                                                AppointmentRequest.status == 0)).gino.all()
    # 1. Request all corresponding DateRanges
    # 2. Map active_requests with a schema
    # 3. Create AppointmentRequestCreate, set parameters from AppointmentRequest schema
    # 4. Set list from (1)
    active_requests_return: t.List[AppointmentRequestUpdate]
    active_requests_return = []
    for req in active_requests:
        dr = await DateSlot.query.where(DateSlot.appointment_request_id == req.id).gino.all()
        new_ar = AppointmentRequestUpdate(doctor_id=req.doctor_id, specialisation_id=req.specialisation_id,
                                          name=req.name, surname=req.surname,
                                          middlename=req.middlename,
                                          email=req.email, phone=req.phone,
                                          id=req.id)
        for x in dr:
            date_range = DateRange(start_datetime=x.start_datetime, end_datetime=x.end_datetime)
            new_ar.time_slots.append(date_range)
        active_requests_return.append(new_ar)
    return active_requests_return


async def change_request_status(request: AppointmentRequestUpdate, status=1) -> AppointmentRequestUpdate:
    req = await AppointmentRequest.query.where(AppointmentRequest.id == request.id).gino.first()

    if req is None:
        raise HTTPException(404, 'Appointment not found')

    await req.update(**{"is_active": False, "status": status}).apply()

    return request


async def get_appointments(doctor_id: int) -> t.List[AppointmentRequestUpdate]:
    reqs = await AppointmentRequest.query.where(and_(AppointmentRequest.doctor_id == doctor_id,
                                                     AppointmentRequest.status == 1)).gino.all()

    new_reqs = list(map(lambda x: AppointmentRequestUpdate(doctor_id=x.doctor_id, specialisation_id=x.specialisation_id,
                                                           name=x.name, surname=x.surname, middlename=x.middlename,
                                                           email=x.email, phone=x.phone, id=x.id), reqs))
    for req in new_reqs:
        dr = await DateSlot.query.where(DateSlot.appointment_request_id == req.id).gino.all()
        for x in dr:
            date_range = DateRange(start_datetime=x.start_datetime, end_datetime=x.end_datetime)
            req.time_slots.append(date_range)
    return new_reqs


async def prescribe_medication(prescription: PrescriptionCreate) -> PrescriptionCreate:
    await Prescriptions.create(**prescription.dict())
    return prescription


async def get_prescriptions_by_doctor(doctor_id: int) -> t.List[PrescriptionCreate]:
    return await Prescriptions.query.where(Prescriptions.doctor_id == doctor_id).gino.all()


async def get_prescriptions_by_patient(patient_id: int) -> t.List[PrescriptionCreate]:
    return await Prescriptions.query.where(Prescriptions.patient_id == patient_id).gino.all()


async def prescribe_procedure(procedure: ProcedureCreate) -> ProcedureCreate:
    await Procedures.create(**procedure.dict())
    return procedure


async def get_procedures_by_doctor(doctor_id: int) -> t.List[ProcedureCreate]:
    return await Procedures.query.where(Procedures.doctor_id == doctor_id).gino.all()


async def get_procedures_by_patient(patient_id: int) -> t.List[ProcedureCreate]:
    return await Procedures.query.where(Procedures.patient_id == patient_id).gino.all()


async def get_appointments_by_email(email: str) -> t.List[AppointmentRequestUpdate]:
    reqs = await AppointmentRequest.query.where(and_(AppointmentRequest.email == email,
                                                     AppointmentRequest.status == 1)).gino.all()
    new_reqs = list(map(lambda x: AppointmentRequestUpdate(doctor_id=x.doctor_id, specialisation_id=x.specialisation_id,
                                                           name=x.name, surname=x.surname, middlename=x.middlename,
                                                           email=x.email, phone=x.phone, id=x.id, time_slots=[]), reqs))

    for req in new_reqs:
        dr = await DateSlot.query.where(DateSlot.appointment_request_id == req.id).gino.all()
        for x in dr:
            date_range = DateRange(start_datetime=x.start_datetime, end_datetime=x.end_datetime)
            req.time_slots.append(date_range)
        #new_reqs.append(req)
    return new_reqs


async def get_patients_for_period(start_date: str, end_date: str) -> t.List[PatientPublic]:
    start_date = datetime.datetime.strptime(start_date, '%Y-%m-%d')
    end_date = datetime.datetime.strptime(end_date, '%Y-%m-%d')
    patients = await Patient.query.where(
        Patient.registration_date.between(func.date(start_date), func.date(end_date))).gino.all()
    return [PatientPublic.from_orm(x) for x in patients]


async def get_procedures_for_period(start_date: str, end_date: str) -> t.List[ProcedureCreate]:
    procedures = await Procedures.query.where(
        and_(Procedures.date >= start_date, Procedures.date <= end_date)).gino.all()
    return [ProcedureCreate.from_orm(x) for x in procedures]


async def get_medications_for_period(start_date: str, end_date: str) -> t.List[PrescriptionCreate]:
    prescriptions = await Prescriptions.query.where(and_(Prescriptions.start_date >= start_date,
                                                         Prescriptions.start_date <= end_date)).gino.all()
    return [PrescriptionCreate.from_orm(x) for x in prescriptions]


async def get_appointments_for_period(start_date: str, end_date: str) -> t.List[DateSlot]:
    # res = await db.status(db.text('select * from date_slot as d join appointment_request as a on d.appointment_request_id=a.id where a.status=1 and d.start_datetime >= :start_date and d.start_datetime <= :end_date'), {'start_date' : start_date,'end_date' : end_date})
    # print(res)
    # return []
    reqs = await AppointmentRequest.query.gino.all()
    new_reqs = []
    for req in reqs:
        dr = await DateSlot.query.where(DateSlot.appointment_request_id == req.id).gino.all()
        for x in dr:
            date_range = DateRange(start_datetime=x.start_datetime, end_datetime=x.end_datetime)
            req.time_slots.append(date_range)
        new_reqs.append(req)
    return new_reqs


async def get_user_messages(iin: str) -> t.List[Message]:
    messages = await MSG.query.gino.all()
    messages_filter = list(filter(lambda x: x.sender_iin == iin or x.receiver_iin == iin, messages))
    # messages_filter = messages
    return messages_filter


async def send_message(message: Message) -> Message:
    new_id = uuid4().__str__()
    new_message_params = message.copy()
    new_message_params.id = new_id
    new_message_updated = Message(**new_message_params.dict())
    created_message = await MSG.create(**new_message_updated.dict())
    return created_message
