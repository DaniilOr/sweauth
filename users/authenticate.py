import typing

from jose import jwt
import bcrypt
from datetime import datetime, timedelta
from passlib.context import CryptContext
from typing import Optional
import sys
from .schemas import UserPasswordUpdate, JWTMeta, JWTCreds, JWTPayload, Admin, PatientPublic, DoctorPublic,\
    DoctorInDB,PatientInDB


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
config = {
    "SECRET_KEY":"09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7",
    "JWT_ALGORITHM":"HS256",
    "ACCESS_TOKEN_EXPIRE_MINUTES":30,
    "JWT_TOKEN_PREFIX":"Bearer",
    "JWT_AUDIENCE":"backend:auth",

    }


class Authenticate:
    def create_salt_and_hashed_password(self, *, plaintext_password: str) -> UserPasswordUpdate:
        salt = self.generate_salt()
        hashed_password = self.hash_password(password=plaintext_password, salt=salt)
        return UserPasswordUpdate(salt=salt, password=hashed_password)

    @staticmethod
    def generate_salt() -> str:
        return bcrypt.gensalt().decode()

    @staticmethod
    def hash_password(*, password: str, salt: str) -> str:
        return pwd_context.hash(password + salt)

    @staticmethod
    def verify_password(*, password: str, salt: str, hashed_pw: str) -> bool:
        return pwd_context.verify(password + salt, hashed_pw)

    @staticmethod
    def create_access_token_for_user(
            *,
            user: typing.Union[Admin, PatientInDB, DoctorInDB],
            secret_key: str = str(config['SECRET_KEY']),
            audience: str = config['JWT_AUDIENCE'],
            expires_in: int = config['ACCESS_TOKEN_EXPIRE_MINUTES'],
    ) -> Optional[str]:
        if not user:
            return None
        print(type(user))
        if isinstance(user, Admin):
            role = 'administrator'
        elif isinstance(user, PatientInDB):
            role = 'patient'
        elif isinstance(user, DoctorInDB):
            role = 'doctor'
        else:
            raise NotImplementedError('wrong role')
        jwt_meta = JWTMeta(
            role=role,
            aud=audience,
            iat=datetime.timestamp(datetime.now()),
            exp=datetime.timestamp(datetime.now() + timedelta(minutes=expires_in)),
        )
        jwt_creds = JWTCreds(username=user.username) if isinstance(user, Admin) else JWTCreds(username=user.iin)
        token_payload = JWTPayload(
            **jwt_meta.dict(),
            **jwt_creds.dict(),
        )
        return jwt.encode(
            token_payload.dict(), secret_key, algorithm=config['JWT_ALGORITHM']
        )

    @staticmethod
    def decode_access_token(
            access_token: str,
            secret_key: str = str(config['SECRET_KEY']),
            algorithm: str = config['JWT_ALGORITHM'],
            audience: str =  config['JWT_AUDIENCE'],
    ) -> Optional[str]:
        return jwt.decode(
            access_token, secret_key, algorithms=[algorithm],
            audience=audience,
        )
