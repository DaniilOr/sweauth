import datetime

from app.main import db
from users.schemas import AppointmentRequestCreate, DateRange


class Admin(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = "admins"
    id = db.Column(db.BigInteger(), primary_key=True)
    username = db.Column(db.String(), unique=True)
    salt = db.Column(db.Unicode(), nullable=False)
    password = db.Column(db.String(), nullable=False)


class Patient(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = "patients"
    id = db.Column(db.BigInteger(), primary_key=True)
    iin = db.Column(db.String(), unique=True, nullable=False)
    name = db.Column(db.String(), nullable=False)
    surname = db.Column(db.String(), nullable=False)
    middle_name = db.Column(db.String(), nullable=True)
    contact_number = db.Column(db.String(), nullable=False)
    salt = db.Column(db.Unicode(), nullable=False)
    password = db.Column(db.Unicode(), nullable=False)
    government_id = db.Column(db.String(), nullable=False, unique=True)


    day_of_birth = db.Column(db.Date(), nullable=False)
    blood_group = db.Column(db.String(), nullable=False)
    emergency_contact_number = db.Column(db.String(), nullable=False)
    email = db.Column(db.String(), nullable=True)
    address = db.Column(db.String(), nullable=False)
    marital_status = db.Column(db.Boolean(), nullable=False)
    registration_date = db.Column(db.Date(), nullable=False)


class Department(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = "departments"
    id = db.Column(db.BigInteger(), primary_key=True)
    name = db.Column(db.String(), nullable=False, unique=True)


class Specialisation(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = "specialisations"
    id = db.Column(db.BigInteger(), primary_key=True)
    name = db.Column(db.String(), nullable=False, unique=True)


class Schedule(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = "schedule"
    id = db.Column(db.BigInteger(), primary_key=True)

    doctor_id = db.Column(db.BigInteger(), db.ForeignKey('doctors.id'))
    patient_id = db.Column(db.BigInteger(), db.ForeignKey('patients.id'))

    start_datetime = db.Column(db.DateTime(), nullable=False)
    end_datetime = db.Column(db.DateTime(), nullable=False)


class AppointmentRequest(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = "appointment_request"
    id = db.Column(db.BigInteger(), primary_key=True)

    doctor_id = db.Column(db.BigInteger(), db.ForeignKey('doctors.id'))
    specialisation_id = db.Column(db.BigInteger(), db.ForeignKey('specialisations.id'))

    is_active = db.Column(db.Boolean(), nullable=False)
    name = db.Column(db.String(), nullable=False)
    surname = db.Column(db.String(), nullable=False)
    middlename = db.Column(db.String(), nullable=True)
    email = db.Column(db.String(), nullable=True)
    phone = db.Column(db.String(), nullable=True)
    status = db.Column(db.Integer(), nullable=False)



class DateSlot(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = "date_slot"
    id = db.Column(db.BigInteger(), primary_key=True)

    appointment_request_id = db.Column(db.BigInteger(), db.ForeignKey('appointment_request.id'))

    start_datetime = db.Column(db.String(), nullable=False)
    end_datetime = db.Column(db.String(), nullable=False)

    # def __init__(self, appointment_request_id: int, start_datetime: str, end_datetime: str):
    #     self.id = int(datetime.datetime.timestamp(datetime.datetime.now()))
    #     self.appointment_request_id = appointment_request_id
    #     self.start_datetime = start_datetime
    #     self.end_datetime = end_datetime



class Doctor(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = "doctors"
    id = db.Column(db.BigInteger(), primary_key=True)
    iin = db.Column(db.String(), unique=True, nullable=False)
    name = db.Column(db.String(), nullable=False)
    surname = db.Column(db.String(), nullable=False)
    middle_name = db.Column(db.String(), nullable=True)
    contact_number = db.Column(db.String(), nullable=False)
    salt = db.Column(db.Unicode(), nullable=False)
    password = db.Column(db.Unicode(), nullable=False)

    department_id = db.Column(db.BigInteger(), db.ForeignKey('departments.id'))
    specialisation_id = db.Column(db.BigInteger(), db.ForeignKey('specialisations.id'))
    # schedule_id = db.Column(db.BigInteger(), db.ForeignKey('schedules.id'))

    experience = db.Column(db.Integer(), nullable=False)
    price = db.Column(db.Float(), nullable=False)
    rating = db.Column(db.Float(), nullable=False)
    url = db.Column(db.String(), nullable=False)
    day_start = db.Column(db.String(), nullable=False)
    day_end = db.Column(db.String(), nullable=False)
    address = db.Column(db.String(), nullable=False)
    education = db.Column(db.String(), nullable=False)
    photo = db.Column(db.String(), nullable=False)
    category = db.Column(db.String(), nullable=False)
    procedure = db.Column(db.String(), nullable=False)


class Prescriptions(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = "prescriptions"
    id = db.Column(db.BigInteger(), primary_key=True)

    doctor_id = db.Column(db.BigInteger(), db.ForeignKey('doctors.id'))
    patient_id = db.Column(db.BigInteger(), db.ForeignKey('patients.id'))

    name = db.Column(db.String(), nullable=False)
    start_date = db.Column(db.String(), nullable=False)
    end_date = db.Column(db.String(), nullable=False)


class Procedures(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = "procedures"
    id = db.Column(db.BigInteger(), primary_key=True)

    doctor_id = db.Column(db.BigInteger(), db.ForeignKey('doctors.id'))
    patient_id = db.Column(db.BigInteger(), db.ForeignKey('patients.id'))

    name = db.Column(db.String(), nullable=False)
    date = db.Column(db.String(), nullable=False)
    cost = db.Column(db.Integer(), nullable=False)


class Message(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = "messages"
    id = db.Column(db.String(), primary_key=True)
    sender_iin = db.Column(db.String(), nullable=False)
    receiver_iin = db.Column(db.String(), nullable=False)
    send_time = db.Column(db.String(), nullable=False)
    message_text = db.Column(db.String(), nullable=False)

