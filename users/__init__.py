from .authenticate import Authenticate

auth_service = Authenticate()

__all__ = ['auth_service', ]