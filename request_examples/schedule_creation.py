import requests

if __name__ == "__main__":
    import requests

    headers = {
        'accept': 'application/json',
        # Already added when you pass json=
        # 'Content-Type': 'application/json',
    }

    json_data = {"id":0}

    response = requests.post('http://127.0.0.1:8000/users/create_schedule', headers=headers, json=json_data)
    print(response.text)