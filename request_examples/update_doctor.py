import requests

if __name__ == "__main__":
    headers = {
        'accept': 'application/json',
        # Already added when you pass json= but not when you pass data=
        # 'Content-Type': 'application/json',
    }

    json_data = {
        'iin': '0',
        'id': 1,
        'name': 'Boris',
        'surname': 'string',
        'middle_name': 'string',
        'contact_number': 'string',
        'experience': 0,
        'price': 0,
        'rating': 0,
        'url': 'string',
        'address': 'Pushkina 11',
        'education': 'string',
        'photo': 'address/sample',
        'specialisation_id': 1,
        'department_id': 1,
        'schedule_id': 0,
        'password': 'strings',
        "day_start": "1",
        "day_end": "2",
        "category": "dinosaur",
        "procedure": "dog",
        'access_token':
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluQGxvaC5jb20iLCJyb2xlIjoiYWRtaW5pc3RyYXRvciIsImF1ZCI6ImJhY2tlbmQ6YXV0aCIsImlhdCI6MTY3MDA5MzAxNi4yOTI0NTQsImV4cCI6MTY3MDA5NDgxNi4yOTI0NTl9.Kh9vQ57zdQegVqmNmaPDuaRUl-SXrEbdCvsV4S4Bu7s",
    }

    response = requests.put('http://127.0.0.1:8000/users/update_doctor', headers=headers, json=json_data)
    print(response.text)

