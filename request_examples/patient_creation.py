import requests

if __name__ == "__main__":
  headers = {
      'accept': 'application/json',
      # Already added when you pass json= but not when you pass data=
      # 'Content-Type': 'application/json',
  }

  json_data = {
      'iin': '0',
      'name': 'string',
      'surname': 'string',
      'middle_name': 'string',
      'contact_number': 'string',
      'day_of_birth': '2022-11-02',
      'blood_group': 'string',
      'emergency_contact_number': 'string',
      'email': 'user@example.com',
      'address': 'string',
      'marital_status': True,
      'registration_date': '2022-11-02',
      'password': '12345678',
      'government_id': "010022",
      "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluQGxvaC5jb20iLCJyb2xlIjoiYWRtaW5pc3RyYXRvciIsImF1ZCI6ImJhY2tlbmQ6YXV0aCIsImlhdCI6MTY3MDEwMDEwOC4yMzg2NTgsImV4cCI6MTY3MDEwMTkwOC4yMzg2NjN9.NzVh4DH9pl06HWC8L8w7A_Lqg5fhh3Cv_W-cjcPHxIg"
  }
  response = requests.post('http://127.0.0.1:8000/users/create_patinet', headers=headers, json=json_data)
  print(response.text)