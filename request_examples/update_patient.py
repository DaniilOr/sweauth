import requests

if __name__ == "__main__":
  headers = {
      'accept': 'application/json',
      # Already added when you pass json= but not when you pass data=
      # 'Content-Type': 'application/json',
  }

  json_data = {
      'iin': 0,
      'name': 'Igor',
      'surname': 'string',
      'middle_name': 'string',
      'contact_number': 'string',
      'day_of_birth': '2022-11-02',
      'blood_group': 'first',
      'emergency_contact_number': 'string',
      'email': 'user@example.com',
      'address': 'string',
      'marital_status': True,
      'registration_date': '2022-11-02',
      'government_id':'11',
      'password': '12345678',
      'access_token':
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluQGxvaC5jb20iLCJyb2xlIjoiYWRtaW5pc3RyYXRvciIsImF1ZCI6ImJhY2tlbmQ6YXV0aCIsImlhdCI6MTY2OTc4NTcyNy40OTA3MDMsImV4cCI6MTY2OTc4NzUyNy40OTA3MDh9.nmvhsthWG_QRN7-Z8teJXDpY0sC5_3jUdYaLfjUvJKE",
  }
  response = requests.put('http://127.0.0.1:8000/users/update_patient', headers=headers, json=json_data)
  print(response.text)