import requests

if __name__ == "__main__":
    import requests

    headers = {
        'accept': 'application/json',
        # Already added when you pass json= but not when you pass data=
        # 'Content-Type': 'application/json',
    }

    json_data = {
        'name': 'Surgery',
        'id': 5,
        "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluQGxvaC5jb20iLCJyb2xlIjoiYWRtaW5pc3RyYXRvciIsImF1ZCI6ImJhY2tlbmQ6YXV0aCIsImlhdCI6MTY3MDA4MzEwNS4wNjQ3OTcsImV4cCI6MTY3MDA4NDkwNS4wNjQ4MDV9.50rpGT_olDO_TuYGTDcmiJnXODzqer-P3wykaJJJ1DM"
    }

    response = requests.post('http://127.0.0.1:8000/users/create_department', headers=headers, json=json_data)
    print(response.text)