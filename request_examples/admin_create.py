import requests


if __name__ == "__main__":

    headers = {
        'accept': 'application/json',
        # Already added when you pass json= but not when you pass data=
        # 'Content-Type': 'application/json',
    }

    json_data = {
        'id': 2,
        'username': 'admin@loh.com',
        'password': 'qwerty123',
    }

    response = requests.post('http://127.0.0.1:8000/users/add_admin', headers=headers, json=json_data)
    print(response.text)

