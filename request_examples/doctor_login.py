import requests

if __name__ == "__main__":

    headers = {
        'accept': 'application/json',
    }

    json_data = {
        'iin': '2',
        'password': '$2b$12$Ga2ilXhj9/9dge16mSM4SOR1F2r19a77Riq62wjXruSIJupcWk0Xa',
    }

    response = requests.post('http://127.0.0.1:8000/users/login/doctor', headers=headers, json=json_data)
    print(response.text)
