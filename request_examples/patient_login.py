import requests

if __name__ == "__main__":

    headers = {
        'accept': 'application/json',
        # Already added when you pass json= but not when you pass data=
        # 'Content-Type': 'application/json',
    }

    json_data = {
        'iin': '0',
        'password': '12345678',
    }

    response = requests.post('http://127.0.0.1:8000/users/login/patient', headers=headers, json=json_data)
    print(response.text)