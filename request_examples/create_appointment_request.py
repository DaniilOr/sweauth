import requests

if __name__ == "__main__":
    import requests

    headers = {
        'accept': 'application/json',
        # Already added when you pass json= but not when you pass data=
        # 'Content-Type': 'application/json',
    }

    json_data = {
        "doctor_id": 2,
        "specialisation_id": 1,
        "name": "name1",
        "surname": "surname1",
        "middlename": "middlename1",
        "email": "email1",
        "phone": "111111111",
        "time_slots": [
            {
                "start_datetime": "2022-11-27T09:29:40.182Z",
                "end_datetime": "2022-11-27T09:29:40.182Z"
            }
        ]
    }

    response = requests.post('http://127.0.0.1:8000/users/create_appointment_request', headers=headers, json=json_data)
    print(response.text)