import requests

if __name__ == "__main__":
    headers = {
        'accept': 'application/json',
    }
    for i in range(3):
        json_data = {"id": i}
        response = requests.post(
            'http://127.0.0.1:8000/users/create_schedule', headers=headers, json=json_data)
        print(response.text)
    for json_data in [{'id': 1, 'name': 'Neuro-surgery', }, {'id': 2, 'name': 'Cardio-surgery', },
                      {'id': 3, 'name': 'Simple-surgery', }]:
        response = requests.post(
            'http://127.0.0.1:8000/users/create_specialisation', headers=headers, json=json_data)
        print(response.text)

    access_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluQGxvaC5jb20iLCJyb2xlIjoiYWRtaW5pc3RyYXRvciIsImF1ZCI6ImJhY2tlbmQ6YXV0aCIsImlhdCI6MTY3MDMzOTg0OS41NTk1NjIsImV4cCI6MTY3MDM0MTY0OS41NTk1NjZ9.6Xj-pkNor2yzEa_HsaaDsTrbQ-u6YmYkKPjGx8HEPPs"

    for json_data in [{'id': 1, 'name': 'Surgery', "access_token": access_token},
                      {'id': 2, 'name': 'Vision', "access_token": access_token},
                      {'id': 3, 'name': 'Simple-surgery', "access_token": access_token}]:
        response = requests.post(
            'http://127.0.0.1:8000/users/create_department', headers=headers, json=json_data)
        print(response.text)
    for json_data in [
        {
            "iin": "910101100410",
            "name": "John",
            "surname": "Terner",
            "middle_name": "Nick",
            "contact_number": "12345678",
            "id": 1,
            "experience": 1,
            "price": 25000,
            "rating": 2,
            "url": "abc",
            "day_start": "1",
            "day_end": "2",
            "address": "abc",
            "education": "nu",
            "photo": "address/sample",
            "category": "dinosaur",
            "procedure": "dog",
            "specialisation_id": 1,
            "department_id": 1,
            "password": "$2b$12$altKE8/J.DCtAVbvLmquyuwjB9NZbTF9fcaqQglZk9QFsTaJ8EbfK",
            "access_token": access_token
        },
        {
            "iin": "910101100400",
            "name": "Anand",
            "surname": "Kumar",
            "middle_name": "Soora",
            "contact_number": "12345678",
            "id": 2,
            "experience": 1,
            "price": 25000,
            "rating": 2,
            "url": "abc",
            "day_start": "1",
            "day_end": "2",
            "address": "abc",
            "education": "nu",
            "photo": "address/sample",
            "category": "cat",
            "procedure": "cat",
            "specialisation_id": 1,
            "department_id": 1,
            "password": "$2b$12$HJXmWtGZIXIT2KwB5cIz3OUbnPzSlLP0oatd1mv9dCtfyQvL8oEWW",
            "access_token": access_token
        },
        {
            "iin": "1",
            "name": "Tim",
            "surname": "Wang",
            "middle_name": "",
            "contact_number": "12345678",
            "id": 3,
            "experience": 1,
            "price": 25000,
            "rating": 2,
            "url": "abc",
            "day_start": "1",
            "day_end": "2",
            "address": "abc",
            "education": "nu",
            "photo": "address/sample",
            "category": "cat",
            "procedure": "cat",
            "specialisation_id": 1,
            "department_id": 1,
            "password": "$2b$12$QT1Bzd/1tttI.CmkzJDKPes22k3..KRRgAfzI6bQQVmAGgLV6A.hW",
            "access_token": access_token
        },
        {
            "iin": "2",
            "name": "Timothy",
            "surname": "Govern",
            "middle_name": "Lauren",
            "contact_number": "12345678",
            "id": 4,
            "experience": 1,
            "price": 25000,
            "rating": 2,
            "url": "abc",
            "day_start": "1",
            "day_end": "2",
            "address": "abc",
            "education": "nu",
            "photo": "address/sample",
            "category": "dog",
            "procedure": "dog",
            "specialisation_id": 1,
            "department_id": 1,
            "password": "$2b$12$Ga2ilXhj9/9dge16mSM4SOR1F2r19a77Riq62wjXruSIJupcWk0Xa",
            "access_token": access_token
        }
    ]:
        response = requests.post(
            'http://127.0.0.1:8000/users/create_doctor', headers=headers, json=json_data)
        print(response.text)

    for json_data in [
        {
            'iin': '0',
            'name': 'Ivan',
            'surname': 'Petrov',
            'middle_name': '',
            'contact_number': '8800553535',
            'day_of_birth': '2000-11-02',
            'blood_group': 'first',
            'emergency_contact_number': '102',
            'email': 'ivan@petrov.com',
            'address': 'Pushkina',
            'marital_status': True,
            'registration_date': '2022-11-01',
            'password': '12345678',
            'government_id': "010022",
            "access_token": access_token
        },
        {
            'iin': '123',
            'name': 'Petr',
            'surname': 'Ivanov',
            'middle_name': 'Olegovich',
            'contact_number': '11-22-33',
            'day_of_birth': '2010-11-02',
            'blood_group': 'second',
            'emergency_contact_number': '103',
            'email': 'petr@ivanov.com',
            'address': 'Kolotushkina',
            'marital_status': False,
            'registration_date': '2022-11-03',
            'password': '12345678',
            'government_id': "010023",
            "access_token": access_token
        },
        {
            'iin': '1234',
            'name': 'Semen',
            'surname': 'Semenov',
            'middle_name': 'Semenovich',
            'contact_number': '6544321',
            'day_of_birth': '2011-01-02',
            'blood_group': 'first',
            'emergency_contact_number': '111',
            'email': 'aaa@bbb.com',
            'address': 'Mira',
            'marital_status': False,
            'registration_date': '2022-11-01',
            'password': '12345678',
            'government_id': "0100",
            "access_token": access_token
        },
    ]:
        response = requests.post(
            'http://127.0.0.1:8000/users/create_patinet', headers=headers, json=json_data)
        print(response.text)
    print("Done")
