import requests

if __name__ == "__main__":
    headers = {
        'accept': 'application/json',
        # Already added when you pass json= but not when you pass data=
        # 'Content-Type': 'application/json',
    }

    json_data = {
        'iin': '0',
        'name': 'string',
        'surname': 'string',
        'middle_name': 'string',
        'contact_number': 'string',
        'experience': 0,
        'price': 0,
        'rating': 0,
        'url': 'string',
        'address': 'string',
        'education': 'string',
        'photo': 'address/sample',
        'category': 'string',
        'specialisation_id': 1,
        'department_id': 1,
        'schedule_id': 0,
        'password': 'strings',
        "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluQGxvaC5jb20iLCJyb2xlIjoiYWRtaW5pc3RyYXRvciIsImF1ZCI6ImJhY2tlbmQ6YXV0aCIsImlhdCI6MTY2OTYzNzQ1OS4xMjAwNDUsImV4cCI6MTY2OTYzOTI1OS4xMjAwNX0.wUWrsCY320flXNnyaVVCW5BodMbLLdE54FyJYUTsIX0"
    }


    response = requests.post('http://127.0.0.1:8000/users/create_doctor', headers=headers, json=json_data)
    print(response.text)